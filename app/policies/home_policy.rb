class HomePolicy < ApplicationPolicy
  attr_reader :current_user, :post

  def initialize(current_user, post)
    @current_user = current_user
    @post = post
  end

  class Scope
    attr_reader :current_user, :post

    def initialize(current_user, post)
      @current_user = current_user
      @post = post
    end

    def resolve
      post.all
    end
  end
end