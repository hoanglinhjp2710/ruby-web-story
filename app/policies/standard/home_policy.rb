class HomePolicy < ApplicationPolicy
#   attr_reader :current_user, :post
#   def initialize(current_user, post)
#     # raise unless current_user.admin?
#     @current_user = current_user
#     @post = post
#   end
  def index?
    true
  end
  def edit?
    true
  end
  def show?
    true
  end
  def list_waiting_ad_confirm
    user.standard?
  end
  def confirm_public_post
    user.adminnistrator?
  end
  def confirm_post?
    true
    # user.adminnistrator?
  end
  def cancel_post?
    user.adminnistrator?
  end
end