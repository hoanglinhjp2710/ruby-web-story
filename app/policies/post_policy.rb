class PostPolicy < ApplicationPolicy
  attr_reader :current_user, :post

  def initialize(current_user, post)
    @current_user = current_user
    @post = post
  end

  def new?
    @current_user.adminnistrator? || @current_user.standard?
  end

  def create?
    false
  end

  def edit?
    @current_user.standard? || @current_user.adminnistrator?
  end

  def update?
    false
  end

  def show?
    @current_user.standard? || @current_user.adminnistrator?
  end

  def destroy?
    @current_user.adminnistrator? || @current_user.standard?
  end

  def destroy_post_waiting_admin_confirm?
    @current_user.standard?
  end

  def bookmarked?
    @current_user.standard? || @current_user.adminnistrator?
  end

  def liked?
    @current_user.standard? || @current_user.adminnistrator?
  end

  def confirming?
    @current_user.standard?
  end

  def publiced?
    @current_user.standard? || @current_user.adminnistrator?
  end

  def waiting_confirmations?
    @current_user.adminnistrator?
  end

  def confirm_post?
    @current_user.adminnistrator?
  end

  def cancel_post?
    @current_user.adminnistrator?
  end

  class Scope
    attr_reader :current_user, :post

    def initialize(current_user, post)
      @current_user = current_user
      @post = post
    end

    def resolve
      post.all
    end
  end
end