# frozen_string_literal: true

class User::RegistrationService < BaseService
  def create(params)
    user = User.new(params)
    user.role = 'standard'
    user.save!
  end

  def update(user,params)
    user.update(params)
  end
end