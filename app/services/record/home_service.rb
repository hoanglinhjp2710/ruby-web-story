# frozen_string_literal: true

class Record::HomeService < BaseService
  def initialize(user)
    @user = user
  end

  def find_post_by_user
    post = Post.all.where(user_id: @user.id)
  end

  def all_post_active
    post = Post.all.where(status_active: 'active')
  end
end