# frozen_string_literal: true

class Record::LikeService < BaseService
  def initialize(user, params)
    @user = user
    @params = params
  end

  def show
    @like = Like.all.where(user_id: @user.id)
  end

  def toggle_like!
    like = @user.likes.of_post(@params.post_id).first_or_initialize
    if like.persisted?
      like.destroy!
    else
      like.save!
    end
    like
  end
end