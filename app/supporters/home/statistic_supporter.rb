class Home::StatisticSupporter < BaseSupporter
  def total_likes(current_user)
    Like.joins(:post).merge(Post.active).where(user_id: current_user.id).count
  end

  def total_comments(current_user)
    Comment.joins(:post).merge(Post.active).where(user_id: current_user.id).count
  end

  def total_bookmarks(current_user)
    Bookmark.joins(:post).merge(Post.active).where(user_id: current_user.id).count
  end

  def total_posts_of_user_active(current_user)
    Post.joins(:user).merge(Post.active).where(user_id: current_user.id).count
  end

  def total_post_of_user(current_user)
    Post.joins(:user).where(user_id: current_user.id).count
  end

  def list_bookmark(current_user)
    Post.joins(:bookmarks).where(bookmarks: {user_id: current_user.id})
  end

  def list_like(current_user)
    Post.joins(:likes).where(likes: {user_id: current_user.id})
  end


end