# frozen_string_literal: true

class User::RegistrationParams < BaseParams
  def sign_up_params
    params.require(:user).permit(:username, :email, :password, :password_confirmation)
  end

  def update_params
    params.require(:user).permit(:username, :avatar)
  end

  def password_params
    params.require(:user).permit(:password, :password_confirmation)
  end
end
