class Record::CommentParams < BaseParams
  def configure_comment_params
    params.require(:comment).permit(:content)
  end
end