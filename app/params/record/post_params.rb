class Record::PostParams < BaseParams

  def params_id
    params[:id]
  end

  def params_format
    params[:format]
  end

  def configure_create_params
    params.require(:post).permit(:title, :content, :cover_image)
  end

  def update_params
    params.require(:post).permit(:title, :content, :cover_image)
  end

  def update_image_params
    params.require(:post).permit(:cover_image)
  end
  def post_id
    params[:post_id]
  end
end