
import "@fortawesome/fontawesome-free/js/all";
// BEGIN: Import fontawesome svg
import { library, dom } from '@fortawesome/fontawesome-svg-core'
import { faUserSecret, faUnlock } from '@fortawesome/free-solid-svg-icons'
;

library.add(faUserSecret, faUnlock)
dom.watch()
