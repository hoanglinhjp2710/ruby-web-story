class CommentController < BaseController
  def create
    @comment = Comment.new(parameters.configure_comment_params.merge(post_id: params[:post_id], user_id: current_user.id))
    if
      @comment.save!
      flash[:success] = 'thanh cong'
      redirect_to root_path
    else
      flash[:error] = 'error!'
      redirect_to root_path
    end

  end
  def show
    @comment = Comment.all.where(post_id: params[:post_id])
  end

  private
  def parameters
    @params = Record::CommentParams.new params
  end
end
