class PostsController < DashboardController
  # def index
  #   @pagy, @post = pagy(Post.active, items: 3)
  #   @comment = Comment.new
  # end

  def new
    authorize @post = Post.new
  end

  def create
      @post, @message = post_service.create
      flash[:success] = @message
      redirect_to new_post_path
  end

  def edit
     @post = post_service.find_post
     authorize @post
  end

  def update
      post = post_service.find_post
      post_service.update(post)
      flash[:success] = I18n.t(".post.update.success")
      redirect_to edit_post_path
    rescue Exception => e
      flash[:error] = "error: #{e.message}"
      render edit_post_path
  end

  def show
    @comment = Comment.new
   authorize @post = post_service.find_post
  end

  def destroy
    authorize @post = post_service.find_post
    @post.destroy
    post_service.post_destroy
    post_service.like_destroy
    post_service.bookmark_destroy
    post_service.comment_destroy
    flash[:success] = I18n.t(".post.destroyed.success")
    redirect_to publiced_posts_path
  end

  def destroy_post_waiting_admin_confirm
    authorize @post = post_service.find_post
    @post.destroy
    flash[:success] = I18n.t(".post.destroyed.success")
    redirect_to confirming_posts_path
  end

  def bookmarked
    authorize post = post_service.posts_bookmarked
    @pagy, @post = pagy(post , items: 3)
  end

  def liked
    authorize post = post_service.posts_liked
    @pagy, @post = pagy(post, items: 3)
  end

  def confirming
    authorize post = post_service.find_post_confirm_of_current_user
    @pagy, @post = pagy(post, items: 3)
  end

  def publiced
    authorize post = post_service.find_post_active_of_current_user
    @pagy, @post = pagy(post, items: 3)
  end

  def waiting_confirmations
    authorize post = post_service.list_confirm
      @pagy, @post = pagy(post, items: 3)
  end

  def confirm_post
      authorize @post = post_service.find_post
      post_service.confirm_post(@post)
      flash[:success] = I18n.t(".post.active.success")
      redirect_to waiting_confirmations_posts_path
    rescue Exception => e
      flash[:error] = I18n.t(".post.active.errors")
      redirect_to waiting_confirmations_posts_path
  end

  def cancel_post
      authorize post = post_service.find_post
      post_service.cancel_post(post)
      flash[:success] = I18n.t(".post.cancel.success")
      redirect_to waiting_confirmations_posts_path
    rescue Exception => e
      flash[:error] = I18n.t(".post.cancel.errors")
      redirect_to waiting_confirmations_posts_path
  end

  private
  def parameters
    @params ||= Record::PostParams.new params
  end

  def post_service
    post_service ||= Record::PostService.new(current_user, parameters)
  end
end
