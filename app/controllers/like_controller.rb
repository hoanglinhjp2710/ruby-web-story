class LikeController < BaseController
  def create
    @like = like_service.toggle_like!
  end

  def show
    @like = Like.all.where(user_id: current_user.id)
  end

  private
  def parameters
    @params = Record::PostParams.new params
  end

  def like_service
    @like_service ||= Record::LikeService.new(current_user, parameters)
  end
end
