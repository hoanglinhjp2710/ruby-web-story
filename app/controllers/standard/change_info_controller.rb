module Standard
  class ChangeInfoController < BaseController
    def change_password
      @user = current_user
    end

    def change_password_put
        service.update(current_user,parameters.password_params)
        flash[:success] = I18n.t(".devise.passwords.updated")
        redirect_to new_user_session_path
      rescue Exception => e
        flash[:error] = "error: #{e.message}"
        redirect_to change_password_path
    end
    private
    def parameters
      @params ||= User::RegistrationParams.new params
    end
    def service
      service ||= User::RegistrationService.new
    end
  end
end
