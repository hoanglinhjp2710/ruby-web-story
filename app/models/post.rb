class Post < ApplicationRecord
  enum status_active: {disactive: 0,confirm: 1, active: 2}
  has_one_attached :cover_image
  belongs_to :user
  has_many :likes
  has_many :users, through: :likes
  has_many :bookmarks
  has_many :users , through: :bookmarks
  has_many :comments
  has_many :users, through: :comments
  validates :title , presence: true
  validates :content , presence: true
  validates :status_active , presence: true, inclusion: {in:status_actives.keys}
  validates :cover_image, presence: true
  # scope :search_for_title, -> (search){ where("title LIKE ?", "%#{search}%") if search.present?}
  # scope :search_for_content, ->(search) { where("content LIKE ?", "%#{search}%") if search.present?}
  # Ex:- scope :active, -> {where(:active => true)}
  # Ex:- scope :active, -> {where(:active => true)}
end
