class Like < ApplicationRecord
  belongs_to :post
  belongs_to :user

  scope :of_post, ->(post_id) { where post_id: post_id }
end
