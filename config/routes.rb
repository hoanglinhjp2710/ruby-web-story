Rails.application.routes.draw do
  scope "(:locale)", locale: /en|vi/ do
    resources :posts do
      resources :comment, only: [:new, :create]
      resources :like, only: [:create]
      resources :book_mark, only: [:create]

      # RESOURCE, SCOPE
      # /CONTROLLERS/:ID/METHOD
      member do
        delete 'delete', to: 'posts#destroy_post_waiting_admin_confirm'
        post 'confirm', to: 'posts#confirm_post'
        post 'cancel', to: 'posts#cancel_post'
      end

      # /CONTROLERS/METHOD
      collection do
        get :bookmarked, to: 'posts#bookmarked'
        get :liked, to: 'posts#liked'
        get :confirming, to: 'posts#confirming'
        get :waiting_confirmations , to: 'posts#waiting_confirmations'
        get :publiced , to: 'posts#publiced'
      end
    end

    namespace :standard do
      resources :home
    end

    devise_scope :user do
      get 'password/change', to: 'standard/change_info#change_password', as: :change_password
      put 'password/change', to: 'standard/change_info#change_password_put', as: :change_password_put
      get 'profile', to: 'standard/home#show', as: :profile
    end

    devise_for :user, path: 'auth',
      path_names: { sign_in: 'login', sign_out: 'logout', password: 'secret',
                    confirmation: 'comfirm', unlock: 'unblock', registration: 'register',
                    sign_up: 'sign-up'
                  },
      controllers:{registrations: 'user/registrations',
                    sessions: 'user/sessions',
                    passwords: 'user/passwords',
                    unlocks: 'user/unlocks',
                    confirmation: 'user/confirmations'
                  }
  end
  get '/:locale' => 'standard/home#index', as: :root
end
